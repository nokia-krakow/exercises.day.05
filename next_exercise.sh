#
# author: mateusz.franczyk@nokia.com
# date: 20.06.2018
#

function diff_filename()
{
    prefix="local_changes_"
    suffix="01"

	number=0
	while test -e "$prefix$suffix.diff"; do
		(( ++number ))
		suffix="$( printf -- '%02d' "$number" )"
	done

    echo "$prefix$suffix.diff"
}

function branch_checkout()
{
    if [[ `git status --porcelain` ]]; then
        diff_file=$(diff_filename)

        git diff > $diff_file
        git add $diff_file
        git commit -m "save local changes in diff file: $diff_file"

        git reset --hard HEAD
    fi

    git branch -l | grep -F $1 > /dev/null
    if [ $? -ne 0 ]; then
        git checkout -b $1 origin/$1 > /dev/null
    else
        git checkout $1
    fi

    git pull
}


if [ $# -ne 1 ]; then
    echo invalid number of arguments, please retry and pass only exercise name; exit 1
fi

git branch -r | grep -F "origin/$1" > /dev/null
if [ $? -ne 0 ]; then
    echo invalid exercise name, please retry ; exit 1
fi

if [[ $(git rev-parse --abbrev-ref HEAD) = $1 ]]; then
    echo already on branch: $1, please type different name ; exit 1
fi

branch_checkout $1

